import React ,{useState,useEffect} from 'react';
import ReactDOM from 'react-dom';

function Issue_form(props:any) 
{
    const [title,setTitle]= useState(props.issue.title);
    const [assignee,setAssignee]= useState(props.issue.user);
    const [date,setDAte] = useState(props.issue.date);
    const [description,setDescription] = useState(props.issue.Description);

    return (
        <div className="Issue-form" >
            
            <div className="one-field">
            <p>Tilte:</p>
            <input className="inputs" type="text" name="title" value={title} onChange={event => {
                setTitle(event.target.value);
            }}/>
            </div>
            <div className="one-field">
            <p>Assignee:</p>
            <input className="inputs" type="text" name="aassignee" value={assignee} onChange={event => {
                setAssignee(event.target.value);
            }}/>
            </div>
            <div className="one-field">
            <p>Date:</p>
            <input className="inputs" type="text" name="date" value={date} onChange={event => {
                setDAte(event.target.value);
            }}/>
            </div>
            <div className="one-field">
            <p>Description:</p>
            <input className="inputs" type="text" name="description" value={description} onChange={event => {
                setDescription(event.target.value);
            }}/>
            </div>
            <div className="one-field">
            <p>Sprint:</p>
            <input className="inputs" name="comments" />
            </div>
            <div className="one-field">
            <p>Description</p>
            <input className="inputs" name="assignee"  />
            </div>
            <div className="one-field">
            <p>Story points:</p>
            <input className="inputs" name="comments" />
            </div>
            <div className="one-field">
            <p>Attachment</p>
            <input className="inputs" name="assignee" />
            </div>
            <button className="submit-button" type="submit" onClick={() => props.submitHandler(props.issue,title,assignee,date,description)}>Submit   &nbsp; &gt;</button>
            
        </div>
    )
}

export default Issue_form;