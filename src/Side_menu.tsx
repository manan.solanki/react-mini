import React from 'react';

function Side_menu(){
    return (
        <div className="side-menu" id="side_menu">
          <div className="btn">
            <button className="btn-0" id="side_menu_close_button">Close</button>
          </div>
          <button className="btn-1">Dashboard</button>
          <button className="btn-2" id="iss">Issues</button>
          <button className="btn-3">Create</button>
        </div>
    )
}

export default Side_menu;