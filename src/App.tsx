import React, { useState, useEffect } from 'react';
import Modal from 'react-modal';
import data from './mock.json';
import './App.css';
import Card from './Card'
import Issue_form from './Issue_form';
import Header from './Header';
import Side_menu from './Side_menu';
import Issue_static from './Issue_static';



Modal.setAppElement("#root");

function App() {


  const [count_Todo, setCount_Todo] = useState(0);
  const [count_Progress, setCount_Progress] = useState(0);
  const [count_Done, setCount_Done] = useState(0);
  const [issues, setIssues] = useState(data.map(issue => issue));
  const [modalshow,setModalshow] = useState(false);
  const [editIssue,setEditIssue] = useState();

  useEffect(() => {
    let ct = 0;
    let cp = 0;
    let cd = 0;
    issues.map(issue => {
      if (issue.status == "To_do")
        ct++;
      else if (issue.status == "In_progress")
        cp++;
      else if (issue.status == "Done")
        cd++;
    })

    setCount_Todo(ct);
    setCount_Progress(cp);
    setCount_Done(cd);
  }, [issues,modalshow])

  
  const IssueEditHandler = (edit_issue:any) =>
  {
    setEditIssue(edit_issue);
    setModalshow(true);
  }

  const IssueEditSubmitHandler =(issue:any,title:string,assignee:string,date:string,description:string) =>
  {
    issue.title=title;
    issue.user=assignee;
    issue.date=date;
    issue.Description=description;
    setModalshow(false);
    setEditIssue(editIssue);
  }

  const IssueChnageStatus = (issue:any) => 
  {
    if(issue.status=="To_do")
    {
      issue.status="In_progress";
      issue.priority="In Progress";
    }
    else if(issue.status=="In_progress")
    {
      issue.status="Done";
      issue.priority="Done";
    }
    setEditIssue(issue);
  }
 
  return (
    <div className="App">
      <Header />

      <div className="main-container">
        <Side_menu />

        <div className="issue_static" id="issue_static">
          <Issue_static />

          <div className="content">
            <div className="Columns" id="to-do"><p className="col_head">To do {count_Todo}</p>
              {
                issues.map(issue => {
                  if (issue.status == "To_do")
                    return <Card issue={issue} key={issue._id} editHandler={IssueEditHandler} changeStatus={IssueChnageStatus}/>
                })
              }
            </div>
            <div className="Columns" id="in-progress"><p className="col_head">In Progress {count_Progress}</p>
              {
                issues.map(issue => {
                  if (issue.status == "In_progress")
                    return <Card issue={issue} key={issue._id} editHandler={IssueEditHandler}  changeStatus={IssueChnageStatus}/>
                })
              }
            </div>
            <div className="Columns" id="done"><p className="col_head">Done {count_Done}</p>
              {
                issues.map(issue => {
                  if (issue.status == "Done")
                    return <Card issue={issue} key={issue._id} editHandler={IssueEditHandler}  changeStatus={IssueChnageStatus}/>
                })
              }
            </div>
          </div>
        </div>
      </div>
      <Modal className="modal" isOpen={modalshow} onRequestClose={()=>setModalshow(false)}>
        <Issue_form issue={editIssue} submitHandler={IssueEditSubmitHandler}/>
      </Modal>
    </div>
  );
}

export default App;
