import React from 'react';
import ReactDOM from 'react-dom';
import { isPropertySignature } from 'typescript';

function Card(props:any) {

    let status_card: string;
    if (props.issue.status == "To_do") {
        if (props.issue.priority == "High Priority")
            status_card = "issue-priority card-high";
        else
            status_card = "issue-priority card-low";
    }
    else if (props.issue.status == "Done")
        status_card = "issue-priority card-done";
    else
        status_card = "issue-priority card-inprogress"

    return (
    <div className="card" onClick={() => props.editHandler(props.issue)}>
        <div>
            <span className="issue-id">ID : {props.issue._id}</span>
            <span className="issue-date">{props.issue.date}</span>
        </div>
        <p className="issue-title">{props.issue.title}</p>
        <p className="descript">{props.issue.Description}</p>
        <div >
            <span className="assignee">Assignee</span>
            <span className="status">status</span>
        </div>
        <div className="user-profile" >
            <div className="assignee-photo"></div>
            <div className="assignee-tag">
                <p className="assignee-name">{props.issue.user}</p>
                <p className="assignee-post">UI devloper</p>
            </div>

            <button className={status_card} onClick={() => props.changeStatus(props.issue)}>{props.issue.priority}</button>
        </div>
    </div>

  )
}

export default Card;