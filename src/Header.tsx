import React from 'react';

function Header(){
    return (
        <div className="header">
        <button className="menu" id="side_menu_button"></button>
        <div className="logo"></div>
        <div className="search-icon">
          
        </div>

        <div className="on-right">
          <div>
            <a href="#" className="notify">
              <div className="notify">
                
              </div>
              <span className="bell">3</span>
            </a>
          </div>

          <div className="search-mobile-icon">
            
          </div>
          <div className="on-top-right">
            
          </div>
          <div className="menu-top-right"></div>
        </div>
      </div>
    )
}

export default Header;